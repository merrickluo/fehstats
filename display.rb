# frozen_string_literal: true

require 'redis'

# convert data to display
module Display
  @redis = Redis.new(db: 'fehstats')

  def self.build_variation(variation)
    vline = variation.reduce('') do |l, (k, v)|
      modifier =
        case v
        when 1
          k.upcase + '+ '
        when -1
          k.upcase + '- '
        else
          ''
        end
      l + modifier
    end
    "*Stats variation*: `#{vline}`"
  end

  def self.build_table(hero)
    lines = []
    lines << "*#{hero['name']['en']}*, " \
             "*#{hero['name']['ja']}*"
    lines << "*#{hero['movement'].capitalize}*, " \
             "*#{hero['color'].capitalize}*, " \
             "*#{hero['weapon_type'].capitalize}*"

    lines << build_variation(hero['stats_variation'])

    lines << '`' + ' ' * 9 +
             'HP'.rjust(4) +
             'ATK'.rjust(4) +
             'SPD'.rjust(4) +
             'DEF'.rjust(4) +
             'RES'.rjust(4) + '`'

    hero['stats'].each do |k, v|
      star = k.scan(/s(\d)/).first.first
      lv = k.scan(/lv(\d+)/).first.first
      equipped = k.include?('equipped') ? 'E' : ' '
      lines << '`' \
               "☆#{star.to_s.ljust(2)}" \
               "L#{lv.to_s.ljust(3)}" \
               "#{equipped}" \
               "#{v.map { |_, vv| vv.to_s.rjust(4) }.join('')}" \
               '`'
    end

    lines.join("\n")
    # "*#{hl}*```#{stats.join("\n")}```"
  end

  # TODO: show with image
  def self.show(hero)
    text = @redis.get hero['id']
    if text.nil?
      text = build_table(hero)
      @redis.set hero['id'], text
    end
    text
  end
end
