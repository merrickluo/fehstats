# frozen_string_literal: true

require 'json'
require 'net/http'
require 'rufus-scheduler'

# data loading and query
module Database
  @heros = {}

  def self.find_heros(name_key)
    query = /#{name_key}/i
    @heros.select do |hero|
      hero['name']['en'].match?(query) || hero['name']['ja'].match?(query)
    end
  end

  def self.exact_hero(name)
    @heros.detect do |hero|
      hero['name']['en'].casecmp(name).zero? ||
        hero['name']['ja'].casecmp(name).zero?
    end
  end

  def self.reload
    url = URI.parse('https://gitlab.com/merrickluo/fehstats/raw/master/data.json')
    resp = Net::HTTP.get(url)
    raw_data = JSON.parse(resp)

    @heros = raw_data.values
  rescue StandardError => e
    puts 'error reloading data', e
  end

  @scheduler = Rufus::Scheduler.new
  @scheduler.every '10m' do
    puts 'fetching updated data.json'
    reload
    puts 'data updated'
  end

  # reload at start
  reload
end
