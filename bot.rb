# frozen_string_literal: true

require 'telegram/bot'

load 'database.rb'
load 'display.rb'

Types = Telegram::Bot::Types

def on_exact_message(api, message)
  hero = Database.exact_hero(message.text)
  if hero.nil?
    return api.send_message(
      chat_id: message.chat.id,
      text: 'hero not found. type /help to see usage.'
    )
  end
  api.send_message(
    chat_id: message.chat.id,
    text: Display.show(hero),
    parse_mode: 'markdown',
    reply_markup: Types::ReplyKeyboardRemove.new(remove_keyboard: true)
  )
end

def on_help_message(api, message)
  api.send_message(
    chat_id: message.chat.id,
    text: "send hero name to view stats, fuzzy search is default\n" \
           'add a `":"` at the end to do exact match',
    parse_mode: 'markdown'
  )
end

def on_empty_message(api, message)
  api.send_message(
    chat_id: message.chat.id,
    text: 'hero not found. type /help to see usage.'
  )
end

def on_list_message(api, message, heros)
  name_mapping =
    if message.text.match?(/^[a-zA-Z0-9?: ]*$/)
      heros.map { |hero| "#{hero['name']['en']}:" }
    else
      heros.map { |hero| "#{hero['name']['ja']}:" }
    end
  keyboards = Types::ReplyKeyboardMarkup.new(
    keyboard: name_mapping.each_slice(2).to_a,
    resize_keyboard: true,
    one_time_keyboard: true
  )
  api.send_message(
    chat_id: message.chat.id,
    text: 'multiple heros found, choose via keyboard',
    resize_keyboard: true,
    reply_markup: keyboards
  )
end

def on_message(api, message)
  return on_empty_message(api, message) if message.text.nil?

  if message.text.end_with?(':')
    message.text[-1] = ''
    return on_exact_message(api, message)
  end

  heros = Database.find_heros message.text
  if heros.nil? || heros.empty?
    on_empty_message(api, message)
  elsif heros.length == 1
    on_exact_message(api, message)
  else
    on_list_message(api, message, heros)
  end
end

def on_inline(api, message)
  # avaid rebuild all hero cache
  return if message.query.nil? || message.query.empty?

  heros = Database.find_heros(message.query)
  results = heros.map do |hero|
    Types::InlineQueryResultArticle.new(
      id: hero['name']['en'],
      title: "#{hero['name']['en']}, #{hero['name']['ja']}",
      input_message_content: Types::InputTextMessageContent.new(
        message_text: Display.show(hero),
        parse_mode: 'markdown'
      )
    )
  end
  api.answer_inline_query(
    inline_query_id: message.id,
    results: results
  )
end

# check for token
token = ENV['TG_TOKEN']
if token.nil? || token.empty?
  puts 'please set environment variable TG_TOKEN'
  exit(1)
end

Telegram::Bot::Client.run(ENV['TG_TOKEN']) do |bot|
  bot.listen do |message|
    case message
    when Types::Message
      case message.text
      when nil?
        on_empty_message(bot.api, message)
      when '/start', '/help'
        on_help_message(bot.api, message)
      else
        on_message(bot.api, message)
      end
    when Types::InlineQuery
      on_inline(bot.api, message)
    end
  rescue Telegram::Bot::Exceptions::ResponseError => e
    puts e
  end
end
