FROM ruby:2.5-alpine

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ADD Gemfile /usr/src/app
ADD Gemfile.lock /usr/src/app

RUN bundle install --without development test

ENV REDIS_URL "redis://redis"
ENV TG_TOKEN ""

ADD . /usr/src/app

CMD ["ruby", "bot.rb"]
